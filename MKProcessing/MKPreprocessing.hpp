/*
 *  MKPreprocessing.hpp
 *  MKPreprocessing
 *
 *  Created by Marcin Karmelita on 04/02/17.
 *  Copyright © 2017 Marcin Karmelita. All rights reserved.
 *
 */

#ifndef MKPreprocessing_
#define MKPreprocessing_

/* The classes below are exported */
#pragma GCC visibility push(default)

class MKPreprocessing
{
	public:
		void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
