//
//  MKProcessing.hpp
//  MKProcessing
//
//  Created by Marcin Karmelita on 06/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#ifndef MKProcessing_hpp
#define MKProcessing_hpp

#include "Processing.hpp"
#include "Property.hpp"



namespace processing {
    using namespace std;
    using namespace cv;

    class MKProcessing {
    public:
        cv::Mat srcImage;
        cv::Mat binarizedImage;
        cv::Mat blocksOfText;
        ROIS rois;
    
        std::vector<cv::Mat> roisMat;
        Property<bool> processFullscreen;
        cv::Rect roi;
        cv::Ptr<cv::ml::SVM> svm;
        std::vector<cv::Rect> rects;
    public:
        MKProcessing();
        ~MKProcessing();
        
    public:
        void readFromFile(std::string filename);
        bool preprocessImage(cv::Mat &image);
        void setROI(cv::Rect rect);
        void setROI(int origin_x, int origin_y, int width, int height);
        void setClassifier(std::string pathForClassifier);
        void unsetROI();
        bool isROISet();
        int detectSymbol(cv::Mat &mat);
        
    private:
        bool insideROI(const cv::Point_<int> &pt);
        void filteredMat(cv::Mat & mat, cv::Rect mask);
        void mock();
        int detectDigit(cv::Mat& mat);
        
    public:
        
        
        
        
    };
}


#endif /* MKProcessing_hpp */
