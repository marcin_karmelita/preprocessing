//
//  MKProcessing.cpp
//  MKProcessing
//
//  Created by Marcin Karmelita on 06/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#include "MKProcessing.hpp"
#include "Processing.hpp"

using namespace processing;

MKProcessing::MKProcessing() {
    MKLogger::getInstance().visualLoggingLevel = VisualVerbose::veryhigh;
    MKLogger::getInstance().loggingLevel = Severity::debug;
    this->processFullscreen = true;
    
    ///TODO: Remove mock asap
    mock();
}

MKProcessing::~MKProcessing() {
    
}

void MKProcessing::readFromFile(std::string filename) {
    try {
        srcImage = readMatFromFile(filename);
    } catch (InnerException &e) {
        LOG(Severity::fatal, e.what());
    }
}

bool MKProcessing::preprocessImage(cv::Mat& image) {
    
    try {
        
//        if (!processFullscreen) {
//            precondition(roi.area(), {
//                throw InnerException("ROI should never be empty.", __FILE__, __FUNCTION__, __LINE__);
//            });
//            
//            filteredMat(image, roi);
//        }
        // detect blocks of text and choose the closest one to the center of mat (image, frame)
        std::vector<cv::Rect> rects;
        processing::preprocess(image, rects);
        cv::Rect centerRect = processing::getCenterROI(rects, cv::Size(image.cols, image.rows));
        // detect single symbols in the ROI
        precondition(centerRect.area(), {
            LOG(Severity::fatal, "ROI cannot be empty!");
            return EXIT_FAILURE;
        });
        cv::Mat roi = processing::extractROI(image, centerRect);
        roi.copyTo(image.rowRange(0, roi.rows).colRange(0, roi.cols));
        processing::preprocessROI(roi);
        roisMat.clear();
        processing::findContours(roi, roisMat);
        for_each(roisMat.begin(), roisMat.end(), [this](cv::Mat &m){
            cv::resize(m, m, cv::Size(28,28));
            try {
                int predictedSymbol = detectSymbol(m);
                            LOG(Severity::silent, (std::ostringstream()<<"Detected symbol: "<< predictedSymbol).str());
            } catch (InnerException &e) {
                LOG(Severity::fatal, e.what());
            }
        });
        
    } catch (std::exception &e) {
        cout << e.what();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

void MKProcessing::setROI(cv::Rect rect) {
    roi = rect;
    this->processFullscreen = false;
}

void MKProcessing::setROI(int origin_x, int origin_y, int width, int heigth) {
    setROI(cv::Rect(origin_x, origin_y, width, heigth));
}

void MKProcessing::unsetROI() {
    this->processFullscreen = true;
}

bool MKProcessing::isROISet() {
    return this->processFullscreen;
}

void MKProcessing::mock() {
    int height = 250;
    setROI(0, (1280-height)/2, 720, height);
}


bool MKProcessing::insideROI(const Point_<int> &pt) {
    return roi.contains(pt);
}

void MKProcessing::filteredMat(cv::Mat &mat, cv::Rect mask) {
    precondition(roi.area(), return);
    
    for (int row = 0; row < mat.rows; row ++) {
        for (int col = 0; col < mat.cols; col ++) {
            if (!mask.contains(cv::Point(col, row))) {
                mat.at<int>(row, col) = 0;
            }
        }
    }
}


void MKProcessing::setClassifier(std::string pathForClassifier) {
    svm = cv::ml::StatModel::load<cv::ml::SVM>(pathForClassifier);
}

int MKProcessing::detectSymbol(cv::Mat &mat) {
    precondition(!svm.empty(), throw InnerException("SVM is empty", __FILE__, __FUNCTION__, __LINE__));
    
    cv::Mat testMat = mat.clone().reshape(1,1);
    testMat.convertTo(testMat, CV_32F);
    LOG(Severity::debug, debugMat(testMat));
    try {
        int predicted = svm->predict(testMat);
        LOG(Severity::debug, (std::ostringstream()<< "Recognizing following number -> " << predicted).str());
        return predicted;
        
    } catch(cv::Exception& e){
        LOG(Severity::fatal, e.what());
        throw InnerException(e.what(), __FILE__, __FUNCTION__, __LINE__);
    }
}


