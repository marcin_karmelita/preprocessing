//
//  Processing.cpp
//  OCRTest
//
//  Created by Marcin Karmelita on 03/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#include "Processing.hpp"
#include <map>
#include <opencv2/photo/photo.hpp>

namespace processing {
    
#pragma mark - Private
    template <typename T>
    bool compare(T &lhs, T &rhs) {
        return lhs < rhs;
    }
    
    template<typename T>
    T radToDegree(T rad) {
        return 180*rad/CV_PI;
    }
    
    template<typename T>
    T degreeToRad(T degree){
        return CV_PI*degree/180;
    }
    
    void rotate(cv::Mat &src, double rad) {
        cv::Mat rotation = cv::getRotationMatrix2D(cv::Point_<double>(src.cols/2, src.rows/2), radToDegree(rad)/2, 1);
        cv::warpAffine(src, src, rotation, src.size(), cv::INTER_CUBIC);
        VISUALLOG(VisualVerbose::meddium, {
            cv::imshow("Rotation", src);
            cv::waitKey();
        });
    }
    
    double deskew(cv::Mat& src) {
        cv::Mat color_dst;
        cvtColor(src, color_dst, cv::COLOR_GRAY2BGR);
        
        auto average = [](std::vector<double>& radians){
            std::sort(radians.begin(), radians.end(), compare<double>);
            double elements = static_cast<double>(radians.size());
            double result = 0.0;
            for (auto radian: radians) {
                result += radian/elements;
                LOG(Severity::debug, (std::stringstream()<<" Theta: "<<radian).str());
            }
            return result;
        };
        
        std::vector<cv::Vec4i> lines;
        HoughLinesP(src, lines, 1, (CV_PI/180), 100, 150, 20);
        std::vector<double> thetas;
        for(auto& hLine: lines) {
            double theta = atan2(hLine[3] - hLine[1], hLine[2] - hLine[0]);
            thetas.push_back(theta);
            LOG(Severity::debug, (std::stringstream()<<"Line: " <<hLine<< " Theta: "<<theta).str());
            VISUALLOG(VisualVerbose::high, {
                line(color_dst, cv::Point(hLine[0], hLine[1]),
                     cv::Point(hLine[2], hLine[3]), cv::Scalar(0,0,255), 3, 8);
            });
        }
        
        VISUALLOG(VisualVerbose::high, {
            cv::namedWindow( "Detected Lines", 1);
            imshow( "Detected Lines", color_dst);
            cv::waitKey(0);
        });
        
        return average(thetas);
    }
    
    
    std::vector<cv::Scalar> getColors(int channels) {
        if (channels == 1) {
            return {cv::Scalar(127,127,127)};
        }
        return {cv::Scalar(255,0,0), cv::Scalar(0,255,0), cv::Scalar(0,0,255)};
    }
    
    void calcHist(cv::Mat& src) {
        /// Separate the image in 3 places ( B, G and R )
        std::vector<cv::Mat> bgr_planes;
        cv::split( src, bgr_planes );
        std::vector<cv::Scalar> colors = getColors((int)bgr_planes.size());
        
        /// Establish the number of bins
        int histSize = 256;
        
        /// Set the ranges ( for B,G,R) )
        float range[] = { 0, 256 } ;
        const float* histRange = { range };
        
        bool uniform = true; bool accumulate = false;
        
        std::vector<cv::Mat> color_hist = {cv::Mat(), cv::Mat(), cv::Mat()};
        
        // Draw the histograms for B, G and R
        int hist_w = 512; int hist_h = 400;
        int bin_w = cvRound( (double) hist_w/histSize );
        
        cv::Mat histImage( hist_h, hist_w, CV_8UC3, cv::Scalar( 0,0,0) );
        
        /// Normalize the result to [ 0, histImage.rows ]
        for (int i=0; i<3; i++) {
            calcHist( &bgr_planes[i], 1, 0, cv::Mat(), color_hist[i], 1, &histSize, &histRange, uniform, accumulate );
            normalize(color_hist[i], color_hist[i], 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat() );
        }
        
        /// Draw for each channel
        for( int i = 1; i < histSize; i++ ) {
            for (int channel = 0; channel <3 ; channel++) {
                line( histImage, cv::Point( bin_w*(i-1), hist_h - cvRound(color_hist[channel].at<float>(i-1)) ) ,
                     cv::Point( bin_w*(i), hist_h - cvRound(color_hist[channel].at<float>(i)) ),
                     colors[channel], 2, 8, 0  );
            }
        }
        
        /// Display
        cv::namedWindow("calcHist Demo", CV_WINDOW_AUTOSIZE );
        cv::imshow("calcHist Demo", histImage );
        
        cv::waitKey(0);
    }
    
    
#pragma mark - ROI
    
    std::ostream &operator << (std::ostream &os, const ROI &roi) {
        return os << "Rect: [" << roi.rect << "], Distance to center: "<<roi.distanceToCenter << ", Area: " <<roi.rect.area();
    }
    
    std::ostream &operator << (std::ostream &os, const ROIS &rois) {
        os << "ROIS: \n";
        for(auto roi: rois) {
            os << roi << ",\n";
        }
        return os;
    }
    
    bool operator < (const ROI &lhs, const ROI &rhs) {
        return lhs.distanceToCenter < rhs.distanceToCenter;
    }
    bool operator > (const ROI &lhs, const ROI &rhs) {
        return lhs.distanceToCenter > rhs.distanceToCenter;
    }
    bool operator <= (const ROI &lhs, const ROI &rhs) {
        return lhs.distanceToCenter <= rhs.distanceToCenter;
    }
    bool operator >= (const ROI &lhs, const ROI &rhs) {
        return lhs.distanceToCenter >= rhs.distanceToCenter;
    }
    bool operator == (const ROI &lhs, const ROI &rhs) {
        return lhs.distanceToCenter == rhs.distanceToCenter;
    }
    
#pragma mark - New API
    
    void segmentizeImage(cv::Mat& mat, ROIS& rois) {
        
        cv::Mat mask = cv::Mat::zeros(mat.size(), CV_8UC1);
        std::vector<std::vector<cv::Point>> contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(mat, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0)); // find contours
        LOG(Severity::debug, (std::ostringstream()<<"Number of contrours: "<< contours.size()).str());
        precondition(contours.size() != 0, return);
        for(int idx = 0; idx >= 0; idx = hierarchy[idx][0]) { // filter contours
            cv::Rect rect = boundingRect(contours[idx]);
            
            cv::Mat maskROI(mask, rect);
            maskROI = cv::Scalar(0, 0, 0);
            cv::drawContours(mask, contours, idx, cv::Scalar(255, 255, 255), CV_FILLED); // fill the contour
            
            if ((double)countNonZero(maskROI)/(rect.width*rect.height) > .45 && (rect.height > 8 && rect.width > 8)) {
                /* ratio of non-zero pixels in the filled region */
                /* assume at least 45% of the area is filled if it contains text */
                /* constraints on region size */
                /* these two conditions alone are not very robust. better to use something
                 like the number of significant peaks in a horizontal projection as a third condition */
                
                rois.push_back({rect, calculateDistance(mat.size(), rect)});
                
                VISUALLOG(VisualVerbose::high, {
                    cv::imshow("MaskROI", maskROI);
                    cv::waitKey(0);
                });
            }
        }
    }
    
    void prepareImage(cv::Mat &binarized, cv::Mat &blocksOfText) {
        cv::Mat grad, bw;
        cv::Mat morphKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3)); // morphological gradient
        cv::morphologyEx(binarized, grad, cv::MORPH_GRADIENT, morphKernel);
        morphKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(100, 1)); // connect horizontally oriented regions
        cv::morphologyEx(grad, blocksOfText, cv::MORPH_CLOSE, morphKernel);
        VISUALLOG(VisualVerbose::high, {
            cv::imshow(kMorph, blocksOfText);
            cv::waitKey(0);
        });
        
        double rot_angle = deskew(blocksOfText);
        rotate(binarized, rot_angle);
        rotate(blocksOfText, rot_angle);
    }
    
    void purifyImage(cv::Mat &mat, cv::Mat &binarized) {
        cv::Mat gray;
        
        if (mat.channels() != 1) {
            cv::cvtColor(mat, gray, CV_BGR2GRAY);
        } else {
            mat.copyTo(gray);
        }
        
        cv::Size size(3,3);
        cv::GaussianBlur(gray,gray,size,0);
        adaptiveThreshold(gray, binarized,255,CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY,175,30);
        
        if (colouredBackground(binarized)) {
            cv::bitwise_not(binarized, binarized);
        }
        
        VISUALLOG(VisualVerbose::meddium, {
            cv::moveWindow("Pure", 0, 0);
            cv::moveWindow("Gray", mat.cols, 0);
            cv::moveWindow("Binarized", mat.cols + gray.cols, 0);
            
            cv::imshow("Pure", mat);
            cv::imshow("Gray", gray);
            cv::imshow("Binarized", binarized);
            
            cv::waitKey(0);
        });
    }
    
#pragma mark - Old API
    
    void preprocessROI(cv::Mat &mat) {
        cv::Mat gray, blurred, threshholded;
        cv::pyrDown(mat, mat);
        cv::cvtColor(mat, gray, CV_BGR2GRAY);
        cv::GaussianBlur(gray, blurred, cv::Size(5, 5), 0);
        cv::threshold(blurred, threshholded, 0, 255, cv::THRESH_OTSU);
        cv::GaussianBlur(threshholded, threshholded, cv::Size(3, 3), 0);
        if (colouredBackground(threshholded)) {
            cv::bitwise_not(threshholded, threshholded);
        }
        threshholded.copyTo(mat);
        VISUALLOG(VisualVerbose::high, {
            cv::imshow(kGray, gray);
            cv::moveWindow(kGray, 0, 0);
            cv::imshow(kBlur, blurred);
            cv::moveWindow(kBlur, 0, gray.rows+100);
        });
        VISUALLOG(VisualVerbose::meddium, {
            cv::imshow(kThresh, threshholded);
            cv::moveWindow(kThresh, 0, 0);
        });
    }
    
    void findContours(cv::Mat &mat, std::vector<cv::Mat>& rois) {
        //TODO: Does not work
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(mat, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE);
        int key = 0;
        cv::Mat mask = cv::Mat::zeros(mat.size(), CV_8UC1);
        for(int idx = 0; idx >= 0; idx = hierarchy[idx][0]) { // filter contours
            cv::Rect rect = boundingRect(contours[idx]);
            
            cv::Mat maskROI(mask, rect);
            maskROI = cv::Scalar(0, 0, 0);
            cv::drawContours(mask, contours, idx, cv::Scalar(255, 255, 255), CV_FILLED); // fill the contour
            rois.push_back(maskROI);
            VISUALLOG(VisualVerbose::meddium, {
                trackbar(maskROI, kRoi);
                cv::imshow(kRoi, maskROI);
                cv::moveWindow(kRoi, 100, 100);
                wait(key, continue);
            });
            
        }
    }
    
    
    
    cv::Mat extractROI(cv::Mat &mat, cv::Rect rect) {
        VISUALLOG(VisualVerbose::verylow, {
            cv::rectangle(mat, rect.tl(), rect.br(), red);
        });
        return mat(rect);
    }
    
    cv::Mat readMatFromFile(std::string filename) {
        cv::Mat image = cv::imread(filename);
        if (image.empty()) {
            throw InnerException("Empty image", __BASE_FILE__, __FUNCTION__, __LINE__);
        }
        return image;
    }
    
    void preprocess(cv::Mat &large, std::vector<cv::Rect> &rects) {
        VISUALLOG(VisualVerbose::veryhigh, {
            drawCircleInCenter(large);
        });
        LOG(Severity::debug, debugMat(large));
        cv::Mat rgb, small, grad, bw, connected;
        cv::pyrDown(large, rgb); // downsample and use it for processing
        cv::cvtColor(rgb, small, CV_RGBA2GRAY);
        cv::Mat morphKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3)); // morphological gradient
        cv::morphologyEx(small, grad, cv::MORPH_GRADIENT, morphKernel);
        cv::threshold(grad, bw, 0.0, 255.0, cv::THRESH_BINARY | cv::THRESH_OTSU); // binarize
        morphKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 1)); // connect horizontally oriented regions
        cv::morphologyEx(bw, connected, cv::MORPH_CLOSE, morphKernel);
        VISUALLOG(VisualVerbose::high, {
            cv::imshow(kThresh, bw);
            cv::imshow(kMorph, connected);
        });
        cv::Mat mask = cv::Mat::zeros(bw.size(), CV_8UC1);
        std::vector<std::vector<cv::Point>> contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0)); // find contours
        LOG(Severity::debug, (std::ostringstream()<<"Number of contrours: "<< contours.size()).str());
        precondition(contours.size() != 0, return);
        for(int idx = 0; idx >= 0; idx = hierarchy[idx][0]) { // filter contours
            cv::Rect rect = boundingRect(contours[idx]);
            
            cv::Mat maskROI(mask, rect);
            maskROI = cv::Scalar(0, 0, 0);
            cv::drawContours(mask, contours, idx, cv::Scalar(255, 255, 255), CV_FILLED); // fill the contour
            
            if ((double)countNonZero(maskROI)/(rect.width*rect.height) > .45 && (rect.height > 8 && rect.width > 8)) {
                /* ratio of non-zero pixels in the filled region */
                /* assume at least 45% of the area is filled if it contains text */
                /* constraints on region size */
                /* these two conditions alone are not very robust. better to use something
                 like the number of significant peaks in a horizontal projection as a third condition */
                
                cv::Rect r = cv::Rect_<int>(rect.x*2, rect.y*2, rect.width*2, rect.height*2);
                rects.push_back(r);
                
                VISUALLOG(VisualVerbose::high, {
                    cv::imshow("Mask ROI", maskROI);
                });
                VISUALLOG(VisualVerbose::low, {
                    cv::rectangle(rgb, rect, green, 2);
                    cv::rectangle(large, r, green, 2);
                    LOG(Severity::debug, "Drawing rectangles.");
                });
                VISUALLOG(VisualVerbose::low, {
                    cv::putText(large, (std::ostringstream() << r).str(), cv::Point(r.x, r.y), CV_FONT_NORMAL, 0.5, red);
                    cv::putText(rgb, (std::ostringstream() << rect).str(), cv::Point(rect.x, rect.y), CV_FONT_NORMAL, 0.5, red);
                    LOG(Severity::debug, "Putting text.");
                });
                
            }
        }
        VISUALLOG(VisualVerbose::high, {
            cv::imshow("d", rgb);
            cv::imshow("ddd", large);
        });
    }
    
    double calculateDistance(cv::Size size, cv::Rect rect) {
        return std::fabs(size.width/2 - (rect.x+rect.width/2)) + abs(size.height/2 - (rect.y+rect.height/2));
    }
    
    void drawCircleInCenter(cv::Mat & mat) {
        cv::circle(mat, cv::Point(mat.cols/2, mat.rows/2), 50, blue);
    }
    
    cv::Rect getCenterROI(std::vector<cv::Rect> & rects, cv::Size imageSize) {
        cv::Rect centerRect;
        int distance = INT_MAX; //initialize with max value
        for (cv::Rect rect: rects) {
            int currentDistance = calculateDistance(imageSize, rect);
            if (distance > currentDistance) {
                distance = currentDistance;
                centerRect = rect;
            }
            LOG(Severity::debug, (std::ostringstream() << "Distance: " << currentDistance << " Rect: " <<rect).str());
        }
        return centerRect;
    }
    
    void trackbar(cv::Mat &mat, std::string windowName, std::string trackbarName, int state, int max) {
        
        cv::TrackbarCallback callback = [](int pos, void* userdata) {
            precondition(pos%2, return);
            cv::Mat mat = *(cv::Mat *)userdata;
            cv::GaussianBlur(mat, mat, cv::Size(pos, pos), 10);
            cv::imshow("matROI", mat);
        };
        
        cv::createTrackbar(trackbarName, windowName, &state, max, callback, (void *)&mat);
    }
    
    void erode(cv::Mat &mat, int element_size) {
        cv::Mat element = getStructuringElement(cv::MORPH_CROSS, cv::Size(2 * element_size + 1, 2 * element_size + 1), cv::Point(element_size, element_size));
        cv::erode(mat, mat, element);
    }
    
    void dilate(cv::Mat &mat, int element_size) {
        cv::Mat element = getStructuringElement(cv::MORPH_CROSS, cv::Size(2 * element_size + 1, 2 * element_size + 1), cv::Point(element_size, element_size));
        cv::dilate(mat, mat, element);
    }
    
    std::string debugMat(cv:: Mat& mat) {
        return (std::stringstream() <<"Size: ["<<mat.cols<<", "<<mat.rows << "] Channels: " <<mat.channels() << " Type: " << getImgType(mat.type())).str();
    }
    
    bool colouredBackground(cv::Mat &mat) {
        precondition(mat.type() == CV_8UC1, throw InnerException("Bad matrix type", __FILE__, __FUNCTION__, __LINE__));
        
        bool upleft = mat.at<uchar>(0,0);
        bool upright = mat.at<uchar>(0, mat.cols-1);
        bool downleft = mat.at<uchar>(mat.rows-1, 0);
        bool downright = mat.at<uchar>(mat.rows-1, mat.cols-1);
        LOG(Severity::debug, debugMat(mat));
        return (upleft + upright + downleft + downright) > 2; // true if more than two corners are not black
    }
    
    std::string getImgType(int imgTypeInt) {
        int numImgTypes = 35; // 7 base types, with five channel options each (none or C1, ..., C4)
        
        int enum_ints[] =       {CV_8U,  CV_8UC1,  CV_8UC2,  CV_8UC3,  CV_8UC4,
            CV_8S,  CV_8SC1,  CV_8SC2,  CV_8SC3,  CV_8SC4,
            CV_16U, CV_16UC1, CV_16UC2, CV_16UC3, CV_16UC4,
            CV_16S, CV_16SC1, CV_16SC2, CV_16SC3, CV_16SC4,
            CV_32S, CV_32SC1, CV_32SC2, CV_32SC3, CV_32SC4,
            CV_32F, CV_32FC1, CV_32FC2, CV_32FC3, CV_32FC4,
            CV_64F, CV_64FC1, CV_64FC2, CV_64FC3, CV_64FC4};
        
        std::string enum_strings[] = {"CV_8U",  "CV_8UC1",  "CV_8UC2",  "CV_8UC3",  "CV_8UC4",
            "CV_8S",  "CV_8SC1",  "CV_8SC2",  "CV_8SC3",  "CV_8SC4",
            "CV_16U", "CV_16UC1", "CV_16UC2", "CV_16UC3", "CV_16UC4",
            "CV_16S", "CV_16SC1", "CV_16SC2", "CV_16SC3", "CV_16SC4",
            "CV_32S", "CV_32SC1", "CV_32SC2", "CV_32SC3", "CV_32SC4",
            "CV_32F", "CV_32FC1", "CV_32FC2", "CV_32FC3", "CV_32FC4",
            "CV_64F", "CV_64FC1", "CV_64FC2", "CV_64FC3", "CV_64FC4"};
        
        for(int i=0; i<numImgTypes; i++) {
            if(imgTypeInt == enum_ints[i]) return enum_strings[i];
        }
        return "unknown image type";
    }
    
    
    
    
    /**
     MOCK
     
     @param mat mat description
     @param pathForClassifier pathForClassifier description
     @param pathForTemplate pathForTemplate description
     @return
     */
    int processing::detectDigit(cv::Mat &mat, std::string pathForClassifier, std::string pathForTemplate) {
        //
        // Load SVM classifier
        cv::Ptr<cv::ml::SVM> svm = cv::ml::StatModel::load<cv::ml::SVM>(pathForClassifier);
        
        // read image file (grayscale)
        cv::Mat imgMat = cv::imread(pathForTemplate, 0);
        
        // convert 2d to 1d
        cv::Mat testMat = imgMat.clone().reshape(1,1);
        testMat.convertTo(testMat, CV_32F);
        
        // try to predict which number has been drawn
        try {
            int predicted = svm->predict(testMat);
            
            std::cout << std::endl  << "Recognizing following number -> " << predicted << std::endl << std::endl;
            
        } catch(cv::Exception ex){
            
        }
        
        return 0;
    }
    
    
    
    
    int detectDigit(cv::Mat &mat) {
        //
        // Load SVM classifier
        cv::Ptr<cv::ml::SVM> svm = cv::ml::StatModel::load<cv::ml::SVM>("classifier.yml");
        
        // read image file (grayscale)
        cv::Mat imgMat = cv::imread("test.jpg", 0);
        
        // convert 2d to 1d
        cv::Mat testMat = imgMat.clone().reshape(1,1);
        testMat.convertTo(testMat, CV_32F);
        
        // try to predict which number has been drawn
        try{
            int predicted = svm->predict(testMat);
            
            std::cout << std::endl  << "Recognizing following number -> " << predicted << std::endl << std::endl;
            
            //            std::string notifyCmd = "notify-send -t 1000 Recognized: " + std::to_string(predicted);
            //            system(notifyCmd.c_str());
            
        }catch(cv::Exception ex){
            
        }
        
        return 0;
    }
    
}
