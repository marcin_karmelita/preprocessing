//
//  Property.hpp
//  MKProcessing
//
//  Created by Marcin Karmelita on 06/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#ifndef Property_hpp
#define Property_hpp

#include <stdio.h>
template <typename T>
class Property {
public:
    virtual ~Property() {}
    virtual T & operator = (const T &f) { return value = f; }
    virtual operator T const & () const { return value; }
    
    virtual void set(const T&f) { value = f;}
    virtual T const & get() const { return value; }
    virtual T mGet() { return value; }
protected:
    T value;
};

#endif /* Property_hpp */
