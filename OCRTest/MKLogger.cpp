//
//  MKLogger.cpp
//  MKProcessing
//
//  Created by Marcin Karmelita on 03/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#include "MKLogger.hpp"

MKLogger& MKLogger::getInstance() {
    static MKLogger instance; // Guaranteed to be destroyed.
    // Instantiated on first use.
    return instance;
}

std::string MKLogger::severitystr(Severity severity) {
    switch (severity) {
        case Severity::debug:
            return "DEBUG";
        case Severity::warning:
            return "WARNING";
        case Severity::fatal:
            return "FATAL";
        case Severity::silent:
            return "SILENT";
    }
}

