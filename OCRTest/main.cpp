#include "MKProcessing.hpp"

void initLogger(std::string file) {
    processing::MKProcessing *imageProcessor = new processing::MKProcessing();
    imageProcessor->readFromFile(file);
    
    processing::purifyImage(imageProcessor->srcImage, imageProcessor->binarizedImage);
    processing::prepareImage(imageProcessor->binarizedImage, imageProcessor->blocksOfText);
    processing::segmentizeImage(imageProcessor->blocksOfText, imageProcessor->rois);
    cv::imshow("src", imageProcessor->srcImage);
    cv::imshow("bina", imageProcessor->binarizedImage);
    cv::imshow("blocks", imageProcessor->blocksOfText);
    
    for(auto &roi: imageProcessor->rois) {
        cv::Mat extractedROI = processing::extractROI(imageProcessor->binarizedImage, roi.rect);
        cv::imshow("ROI", extractedROI);
        std::vector<cv::Mat> rects;
        processing::findContours(extractedROI, rects);
    }
    LOG(Severity::debug, (std::ostringstream()<<imageProcessor->rois).str());
}

int main(int argc, char** argv) {
    
    initLogger(argv[1]);
    //    int key = 0;
    //    for(std::string filename: kFilenames) {
    //        filename = kResourcesDir + filename +kJpg;
    //        try {
    //            cv::Mat image = processing::readMatFromFile(kFilename);
    //            try {
    //                std::vector<cv::Rect> rects;
    //                processing::preprocess(image, rects);
    //                cv::Rect centerRect = processing::getCenterROI(rects, cv::Size(image.cols/2, image.rows/2));
    //                cv::Mat roi = processing::extractROI(image, centerRect);
    //
    //                processing::preprocessROI(roi);
    ////                processing::findContours(roi);
    //
    //            } catch (std::exception &f) {
    //                std::cout << f.what();
    //            }
    //        } catch (std::exception &e) {
    //            std::cout << e.what();
    //        }
    //
    //        wait(key, continue);
    //    }
    //
    return(0);
    
}
