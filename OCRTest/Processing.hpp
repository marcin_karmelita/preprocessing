//
//  Processing.hpp
//  OCRTest
//
//  Created by Marcin Karmelita on 03/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#ifndef Processing_hpp
#define Processing_hpp

#include <stdio.h>
#include <exception>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>

#include "Constants.h"
#include "MKLogger.hpp"


/**
 Macro with allows users to check if the key == 27 was pressed. If so, the expression (expr) is performed.

 @param key int key
 @param expr expression to be performed
 */
#define wait(key, expr) key = cv::waitKey(0); if(key == 27) { expr; }

/**
 Precondition checking if performing the method should be performed or not.

 @param boolean_operation boolean operation
 @param escape expression which is returned if the boolean operation fails
 */
#define precondition(boolean_operation, escape) if(not bool(boolean_operation)) { escape; }

namespace processing {
    struct ROI;
    typedef std::vector<ROI> ROIS;
    
    /**
     Preprocess ROI (Region of Interest). To the input image the following sequence of operation is applied:
     - downsize (OpenCV: pyrDown)
     - convert into greyscale
     - blur
     - threshold
     - blur
     - bitwise (only if necessary)
     
     @param mat input-output matrix
     */
    void preprocessROI(cv::Mat &mat);
    /**
     To the input image are the following applied:
     - downsize (OpenCV: pyrDown)
     - convert into greyscale
     - morphological filters
     - threshold
     And then the ROIs (Regions of Interest) are extracted and returned by reference.
     @param mat input-output matrix
     @param rects vector of rects
     */
    void preprocess(cv::Mat &mat, std::vector<cv::Rect> &rects);
    /**
     Find contours in image.
     Only the external contrours are find.
     
     @param mat input-output matrix
     */
    void findContours(cv::Mat &mat, std::vector<cv::Mat>& rois);
    /**
     Read matrix from the given file.
     
     @param filename path to the file
     @return matrix
     */
    cv::Mat readMatFromFile(std::string filename);
    void dilate(cv::Mat &mat, int element_size);
    /**
     Get the ROI (Region of Interest) located the closest to the center of matrix.
     
     @param rects ROIs (Regions of Interest)
     @param imageSize size of matrix
     @return ROI located to the closest to the center of the matrix
     */
    cv::Rect getCenterROI(std::vector<cv::Rect> & rects, cv::Size imageSize);
    /**
     Draw circle in blue color in the center of the matrix.
     
     @param mat input-output matrix
     */
    void drawCircleInCenter(cv::Mat & mat);
    /**
     Extract ROI (Region of Interest) from given matrix.
     
     @param mat input matrix
     @param rect rectangle within the ROI is located
     @return matrix
     */
    cv::Mat extractROI(cv::Mat &mat, cv::Rect rect);
    /**
     Calculate distance between matrix's center and the ROI (Region of Interest).
     
     @param size Size of matrix
     @param rect roi
     @return distance from the center of matrix to the ROI
     */
    double calculateDistance(cv::Size size, cv::Rect rect);
    
    bool colouredBackground(cv::Mat &mat);
    std::string getImgType(int imgTypeInt);
    cv::Mat readMatFromFile(std::string filename);
    void erode(cv::Mat &mat, int element_size);
    void trackbar(cv::Mat &mat, std::string windowName, std::string trackbarName = "Trackbar", int state = 0, int max = 10);
    std::string debugMat(cv:: Mat& mat);
    int detectDigit(cv::Mat& mat);
    int detectDigit(cv::Mat& mat, std::string pathForClassifier, std::string pathForTemplate);
    
    void purifyImage(cv::Mat &mat, cv::Mat &binarized);
    void prepareImage(cv::Mat &binarized, cv::Mat &blocksOfText);
    
    struct ROI {
        cv::Rect rect;
        double distanceToCenter;
        
        friend bool operator < (const ROI &lhs, const ROI &rhs);
        friend bool operator <= (const ROI &lhs, const ROI &rhs);
        friend bool operator >= (const ROI &lhs, const ROI &rhs);
        friend bool operator > (const ROI &lhs, const ROI &rhs);
        friend bool operator == (const ROI &lhs, const ROI &rhs);
        
        friend std::ostream &operator << (std::ostream &os, const ROI &roi);
        friend std::ostream &operator << (std::ostream &os, const ROIS &rois);
    };
    
    void segmentizeImage(cv::Mat& mat, ROIS& rois);
    
    
    class InnerException: public std::exception {
        std::string message = "Uknown exception! \n";
    public:
        InnerException(){
            
        }
        InnerException(std::string message) {
            this->message = message;
        }
        InnerException(std::string message, std::string file, std::string function, int line) {
            this->message = "Exception: " + message + " At: "+file +"/"+ function+ " : " + std::to_string(line) +"\n";
        }        
        
        virtual const char* what() const throw() {
            return this->message.c_str();
        }
    };
}

#endif /* Processing_hpp */



/*
 
 
 #include "MKProcessing.hpp"
 
 void initLogger() {
 processing::MKProcessing *imageProcessor = new processing::MKProcessing();
 imageProcessor->readFromFile(kFilename);
 //    imageProcessor->preprocessImage(imageProcessor->image);
 processing::purifyImage(imageProcessor->image);
 }
 
 int main(int argc, char** argv) {
 
 initLogger();
 //    int key = 0;
 //    for(std::string filename: kFilenames) {
 //        filename = kResourcesDir + filename +kJpg;
 //        try {
 //            cv::Mat image = processing::readMatFromFile(kFilename);
 //            try {
 //                std::vector<cv::Rect> rects;
 //                processing::preprocess(image, rects);
 //                cv::Rect centerRect = processing::getCenterROI(rects, cv::Size(image.cols/2, image.rows/2));
 //                cv::Mat roi = processing::extractROI(image, centerRect);
 //
 //                processing::preprocessROI(roi);
 ////                processing::findContours(roi);
 //
 //            } catch (std::exception &f) {
 //                std::cout << f.what();
 //            }
 //        } catch (std::exception &e) {
 //            std::cout << e.what();
 //        }
 //
 //        wait(key, continue);
 //    }
 //
 return(0);
 
 }
 

 
 
 */
