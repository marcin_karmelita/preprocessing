//
//  Constants.h
//  OCRTest
//
//  Created by Marcin Karmelita on 03/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
#include <opencv2/core.hpp>

#include <iostream>

const int MIN_CONTOUR_AREA = 50;
const int RESIZED_IMAGE_WIDTH = 20;
const int RESIZED_IMAGE_HEIGHT = 30;

const std::string kGray = "GRAY";
const std::string kBlur = "BLUR";
const std::string kThresh = "THRESHHOLD";
const std::string kMorph = "MORPHOLOGY";
const std::string kFilenames[5] = {"1", "2", "3", "4", "5"};
const std::string kResourcesDir = "/Users/marcinkarmelita/tensorflow/data/real/real";
const std::string kJpg = ".jpg";
const std::string kFilename = "/Users/marcinkarmelita/Downloads/IMG_2273.JPG";
const std::string kWindowName = "Images";
const std::string kRoi = "Mat ROI";

static const cv::Scalar blue = cv::Scalar(255,0,0);
static const cv::Scalar green = cv::Scalar(0,255,0);
static const cv::Scalar red = cv::Scalar(0,0,255);
static const cv::Scalar black = cv::Scalar(0,0,0);
static const cv::Scalar white = cv::Scalar(255,255,255);


#endif /* Constants_h */
