//
//  MKLogger.hpp
//  MKProcessing
//
//  Created by Marcin Karmelita on 03/02/17.
//  Copyright © 2017 Marcin Karmelita. All rights reserved.
//

#ifndef MKLogger_hpp
#define MKLogger_hpp

#include <stdio.h>
#include <iostream>
#include "Property.hpp"
/**
 Log file, method and line where it was executed.
 */
#define BASELOG std::cout <<"File: "<< __FILE__ <<" : "<< __FUNCTION__ <<" : "<< __LINE__;

/**
 Log BASELOG and the message if the condition is satisfied.

 @param Severity level of debugging
 @param message logged message
 */
#define LOG(Severity, message) if((int)Severity <= (int)MKLogger::getInstance().loggingLevel) { std::cout<<MKLogger::severitystr(Severity)<< ": "; \
    BASELOG; \
    std::cout<<" :: "<<message<<std::endl; }

/**
 Show windows depending of the logging (verbose) level.

 @param visualVerbose logging level
 @param expr expression to be performed
 */
#define VISUALLOG(visualVerbose, expr) if((int)visualVerbose <= (int)MKLogger::getInstance().visualLoggingLevel) {(expr);};

typedef enum {
    silent = 0,
    fatal,
    warning,
    debug,
} Severity;

typedef enum {
    none = 0,
    verylow,
    low,
    meddium,
    high,
    veryhigh,
} VisualVerbose;


class MKLogger {
public:
    
    /**
     Get singleton.

     @return singleton
     */
    static MKLogger& getInstance();
    Property<VisualVerbose> visualLoggingLevel;
    Property<Severity> loggingLevel;

private:
    MKLogger(){}
    
public:
    
    MKLogger(MKLogger const&) = delete;
    void operator = (MKLogger const&) = delete;
    
    // Note: Scott Meyers mentions in his Effective Modern
    //       C++ book, that deleted functions should generally
    //       be public as it results in better error messages
    //       due to the compilers behavior to check accessibility
    //       before deleted status
    
    static std::string severitystr(Severity severity);
};

#endif /* MKLogger_hpp */
